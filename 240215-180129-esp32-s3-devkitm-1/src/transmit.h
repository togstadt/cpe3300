#ifndef TRANSMIT_H
#define TRANSMIT_H

#include "main.h"
#include <string>
#include <iostream>


bool setTXWaiting(bool set);

int getCurrentTransmit();

bool setTXEnable(bool request);
bool getTXEnable();

bool getTXWaiting();

int getTXBinMessageSize();
bool getTXBinBuffer();

bool txCheck(hw_timer_t *ManchesterMsgTimer);

bool incrementCurrentTransmit();
bool setCurrentTransmit(int value);
int getTXCollisionDelay();
bool setTXCollisionDelay(int set);

void resetIndex();
void setBin(std::string str);
void stringToBuffer(const char* str);

#endif