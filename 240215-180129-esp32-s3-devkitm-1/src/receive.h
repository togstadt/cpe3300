#ifndef RECEIVE_H
#define RECEIVE_H

#include <Arduino.h>
#include "main.h"
#include "crc.h"
#include "encrypt.h"
#include <string>
#include <iostream>

uint8_t getHeaderPreamble();
uint8_t getHeaderSource();
uint8_t getHeaderDestination();
uint8_t getHeaderLength();
uint8_t getHeaderCRCFlag();

bool getHeaderOver();
int getCrcCount();
bool manMsgSet(int val, int edgeCount);
bool resetHeader();
void printMsg();
bool clearMsg();

#endif