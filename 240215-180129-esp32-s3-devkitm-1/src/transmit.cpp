#include "transmit.h"

bool txBinBuffer[8192];
int txBinMessageSize=0;
bool txEnable = 0;
bool txWaiting = 0;
int currentTransmit=0;
int binaryBufferIndex=0;
int TXCollisionDelay = 0;

bool setTXWaiting(bool set) {
    txWaiting=set;
    return true;
}

bool getTXWaiting() {
    return txWaiting;
}

bool incrementCurrentTransmit() {
    currentTransmit++;
    return true;
}

bool setCurrentTransmit(int value) {
    if(value>=0 && value<=10) {
        currentTransmit=value;
        return true;
    }
    return false;
}

int getCurrentTransmit() {
    return currentTransmit;
}

bool setTXEnable(bool request) {
    txEnable = request;
    return true;
}

bool getTXEnable() {
    return txEnable;
}

int getTXBinMessageSize() {
    return txBinMessageSize;
}

bool getTXBinBuffer() {
    return txBinBuffer[currentTransmit];
}

int getTXCollisionDelay(){
  return TXCollisionDelay;
}

bool setTXCollisionDelay(int set){
    TXCollisionDelay = set;
    return true;
}

bool txCheck(hw_timer_t *ManchesterMsgTimer){
  if(getSYSState() == IDLE) {
    digitalWrite(TRANSMIT_LED, 1);
    timerAlarmEnable(ManchesterMsgTimer);
    txEnable = 1;   // Enable tranmission
    txWaiting = 0;  // Waiting to transmit
    return true;
  }
  return false;
}

void resetIndex() {
  binaryBufferIndex=0;
}

void setBin(std::string str) {
  for(int i=0; i<str.length(); i++) {
    if(str[i]=='1') {
      txBinBuffer[binaryBufferIndex++] = 1;
    } else {
      txBinBuffer[binaryBufferIndex++] = 0;
    }
  }
  txBinMessageSize=binaryBufferIndex;
}

// Convert all characters in myString to 8-bit binary numbers and store them in binaryBuffer.
void stringToBuffer(const char* str){
  // Space for the binary number buffer
  for(int i = 0; i < strlen(str); i++){
    char currentChar = str[i];
    for(int j = 7; j >= 0; j--){
      txBinBuffer[binaryBufferIndex++] = (currentChar >> j) & 1; 
    }
  }
  txBinMessageSize = binaryBufferIndex;
}