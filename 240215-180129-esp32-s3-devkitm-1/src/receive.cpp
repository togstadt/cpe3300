
#include "receive.h"

static uint8_t header[5];
static char message[1500];
static bool flipBit;
static int currentByteCount; // Current number of bytes loaded
static int bitCount; // Current number of bits in byte
static uint8_t crc;
static const uint8_t deviceAddress = 0x34;

bool headerOver=false;
bool getHeaderOver() {
  return headerOver;
}

uint8_t getHeaderPreamble() {
  return header[0];
}

uint8_t getHeaderSource() {
  return header[1];
}

uint8_t getHeaderDestination() {
  return header[2];
}

uint8_t getHeaderLength() {
  return header[3];
}

uint8_t getHeaderCRCFlag() {
  return header[4];
}


bool clearMsg() {
  for(int i=0; i<255; i++) {
    message[i] = '\0';
  }
  return true;
}

// Resets header values to 0 on new message
bool resetHeader() {
  flipBit=1;
  currentByteCount=0;
  bitCount=7;
  
  for(int i=0; i<5; i++) {
    header[i]=0;
  }
  headerOver=false;
  flipBit=!flipBit;
  header[currentByteCount] | (flipBit << bitCount);
  bitCount--;
  return true;
}
int crcCount=0;
int getCrcCount() {
  return crcCount;
}

void writeManMsg() {
  if(currentByteCount<5) {
    header[currentByteCount] |= (flipBit << bitCount);    
  } else {
    message[currentByteCount-5] |= (flipBit << bitCount);
  }
  bitCount--;
}

// Works off of vals of 1 & 2 signifying manchester bit period
bool manMsgSet(int val, int edgeCount) {
  // only valid inputs are 1&2
  switch(val) {
    case 1:
      // only write repeats on odd edgecount
      if(edgeCount%2!=0) {
        writeManMsg();
      }
      break;
    case 2:
      flipBit=!flipBit;
      writeManMsg();
      break;
  }
  // Reset bitcount and increase bytecount
  if(bitCount==-1) {
    bitCount=7;
    currentByteCount++;
    if(currentByteCount>=5) {
      headerOver=true;
    }
  }
  return true;
}

void printMsg() {
  if(getHeaderDestination()==deviceAddress || getHeaderDestination()==0x00) {
    Serial.printf("HEADER: %02x:%02x:%02x:%02x:%02x", 
    header[0],header[1], header[2], header[3], header[4]);
    Serial.println();
    Serial.print("MESSAGE: ");
    decrypt_message(message,strlen(message));
    for(int i=0; i<getHeaderLength(); i++) {
      Serial.print(message[i]);
    }
    Serial.println();

    // Compute CRC8 and compare to received flag
    uint8_t computedCRC = computeCRC8((uint8_t*)message, getHeaderLength());
    Serial.print("Computed CRC: ");
    Serial.printf("%02x",computedCRC);
    Serial.println();
    crc=message[getHeaderLength()];
    Serial.print("Received CRC Trailer: ");
    Serial.printf("%02x",crc);
    Serial.println();
    if(crc!=computedCRC) {
      Serial.println("THERE ARE ERRORS IN THE MESSAGE");
    } else {
      Serial.println("Message received with no errors");
    }
  }
}