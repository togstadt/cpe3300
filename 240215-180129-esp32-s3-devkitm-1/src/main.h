#ifndef MAIN_H
#define MAIN_H
#include <Arduino.h>
#include <HardwareSerial.h>
#include "receive.h"
#include "transmit.h"
#include <string>
#include <iostream>
#include <bitset>
#include "crc.h"
#include "encrypt.h"
// PINS
#define BUSY_LIGHT  5   // Busy light on pin 5
#define IDLE_LIGHT 6      // Idle light on pin 6
#define COLLISION_LIGHT 4  // Collision light on pin 4
#define TRANSMIT_LED 7 //Transmit LED on pin 7
#define RECEIVE  15 // network input line on pin 15
#define TRANSMIT_LINE 16 // network transmit line on pin 16
// CONSTANTS
#define IDLE 0
#define BUSY 1
#define COLLISION 2
#define FIRST 0 // System is in first half-bit
#define SECOND 1 // System is in second half-bit
#define TRANSMIT_TIMER_EN 0
#define STATE_ALARM 1130
#define TX_ALARM 500
#define JSON_USE_IMPLICIT_CONVERSIONS 0

int getSYSState();

#endif