#ifndef ENCRYPT_H
#define ENCRYPT_H

bool create_key();
void create_table();
bool encrypt_message(char* message, int length);
bool decrypt_message(char* message, int length);
bool encode_key();

#endif