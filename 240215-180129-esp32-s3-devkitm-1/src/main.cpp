/*
 * CPE 3300 Network protocol program to sense what state the system is currently in. Uses 3 LEDS to indicate state.
 * Created by Tyler Togstad, Brayden Mleziva, and Xiaohui 1/30/2024
 */
// INCLUDES
#include "main.h"
#include <chrono>

using namespace std::chrono;

// File-Scope vars
int SYS_STATE = IDLE;              // Keeps track of system state
hw_timer_t *IntervalTimer = NULL;  // Timer for system state monitor
hw_timer_t *ManchesterMsgTimer = NULL; // Timer for transmitting a message
hw_timer_t *ReceiveTimer = NULL; // Timer for receiving a message

int bitState = FIRST;             // Keeps track of bit state for transmit interrupt
String message;
String parsedMessage[3];
bool receiving=false;
int timeOfCollision = 0;
bool CollisionWaiting = false;

int getSYSState() {
  return SYS_STATE;
}

// Timer alarm interrupt for 1.13 ms interval
void IRAM_ATTR onTimer() {
  timerStop(IntervalTimer);
  if (digitalRead(RECEIVE) == 1) {
    SYS_STATE = IDLE; // System is in idle
  } else {
    SYS_STATE = COLLISION; // System is in collision
  }
  timerRestart(IntervalTimer);  // Start timer
  timerStart(IntervalTimer);
}

void consoleParser(String fullMessage){
  //find the first space
  int firstSpaceIndex = fullMessage.indexOf(" ");
  //find the second space
  int secondSpaceIndex = fullMessage.indexOf(" ", firstSpaceIndex + 1);
  //find the end of the input message
  int indexOfEnd = fullMessage.indexOf("\n");
  parsedMessage[0] = fullMessage.substring(0,firstSpaceIndex);
  parsedMessage[1] = fullMessage.substring(firstSpaceIndex+1,secondSpaceIndex);
  parsedMessage[2] = fullMessage.substring(secondSpaceIndex+1,indexOfEnd);
  return;
}

void stateUpdate(){
  switch(SYS_STATE) {
    case IDLE:
      digitalWrite(BUSY_LIGHT, 0);
      digitalWrite(IDLE_LIGHT, 1);
      digitalWrite(COLLISION_LIGHT, 0);
      break;
    case BUSY:
      digitalWrite(BUSY_LIGHT, 1);
      digitalWrite(IDLE_LIGHT, 0);
      digitalWrite(COLLISION_LIGHT, 0);
      break;
    case COLLISION:
      digitalWrite(BUSY_LIGHT, 0);
      digitalWrite(IDLE_LIGHT, 0);
      digitalWrite(COLLISION_LIGHT, 1);
      break;
  }
}

void IRAM_ATTR CHANGE_ISR() {
  static int edgeCount;
  static int runCount;
  if(getHeaderDestination() != 0 && 
     getHeaderDestination() != 0x34 && 
     getHeaderDestination() != 0xFF &&  
     getHeaderOver()) {
    receiving=false;
  }
  timerStop(IntervalTimer);  // STOP timer
  timerStop(ReceiveTimer);
  uint64_t currentTime = timerReadMicros(ReceiveTimer);
  if (SYS_STATE == IDLE) {
    SYS_STATE = BUSY; // System is in busy
    edgeCount=0;
    runCount=0;
    resetHeader();
    receiving=true;
  } else {
    SYS_STATE = BUSY; // System is busy
    if(receiving) {
      if(currentTime>750) {
          edgeCount=edgeCount+2;
          manMsgSet(2,edgeCount);
        } else {
          edgeCount=edgeCount+1;
          manMsgSet(1,edgeCount);
        }
    }
  }
  timerRestart(ReceiveTimer);
  timerRestart(IntervalTimer);
  timerStart(IntervalTimer);
  timerStart(ReceiveTimer);
}

// ISR for transmitting data
void IRAM_ATTR SendHalfBit() {
  if(getCurrentTransmit() == getTXBinMessageSize()){
    resetIndex();
    setTXEnable(false);
  } else {
    switch(bitState) { // bitState signifies if it's the first or second half of the bit being sent
      case FIRST: // Case for first half, pull high or low depending on 0 or 1
        switch(getTXBinBuffer()){
          case 0: // 0 starts high
            digitalWrite(TRANSMIT_LINE, 1);
            bitState=SECOND; // Set state for second half of bit to be sent next call
            break;
          case 1: // 1 starts low
            digitalWrite(TRANSMIT_LINE, 0);
            bitState=SECOND; // Set state for second half of bit to be sent next call
            break;
        }
      break;
      case SECOND: // On second state, the ouptut is always inverted from the previous half bit
        digitalWrite(TRANSMIT_LINE, !digitalRead(TRANSMIT_LINE));
        bitState=FIRST; // Set state for next bit to transmit first half
        incrementCurrentTransmit();
        break;
    }
  }
}

void setup() {
  Serial.begin(115200);
  create_key();
  encode_key();
  char* test = "According to all known laws of aviation, there is no way a bee should be able to fly. Its wings are too small to get its fat little body off the ground. The bee, of course, flies anyway because bees don't care what humans think is impossible. Yellow, black. Yellow, black. Yellow, black. Yellow, black. Ooh, black and yellow! Let's shake it up a little. Barry! Breakfast is ready! Ooming! Hang on a second. Hello? - Barry? - Adam? - Oan you believe this is happening? - I can't. I'll pick you up. Looking sharp. Use the stairs. Your father paid good money for those. Sorry. I'm excited. Here's the graduate. We're very proud of you, son. A perfect report card, all B's. Very proud. Ma! I got a thing going here. - You got lint on your fuzz. - Ow! That's me! - Wave to us! We'll be in row 118,000. - Bye! Barry, I told you, stop flying in the house! - Hey, Adam. - Hey, Barry. - Is that fuzz gel? - A little. Special day, graduation. Never thought I'd make it. Three days grade school, three days high school. Those were awkward. Three days college. I'm glad I took a day and hitchhiked around the hive. You did come back different. - Hi, Barry. - Artie, growing a mustache? Looks good. - Hear about Frankie? - Yeah. - You going to the funeral? - No, I'm not going. Everybody knows, sting someone, you die. Don't waste it on a squirrel. Such a hothead. I guess he could have just gotten out of the way. I love this incorporating an amusement park into our day. That's why we don't need vacations. Boy, quite a bit of pomp... under the circumstances. ";
  printf("Plaintext: %s\n", test);
  encrypt_message(test, strlen(test));
  printf("Ciphertext: %s\n", test);
  decrypt_message(test, strlen(test));
  printf("Decrypted: %s\n", test);
  // All lights outputs
  pinMode(BUSY_LIGHT, OUTPUT);
  pinMode(IDLE_LIGHT, OUTPUT);
  pinMode(COLLISION_LIGHT, OUTPUT);
  pinMode(TRANSMIT_LED, OUTPUT);
  // Ethernet comms for now is input only, output will need to be changed on appropriate state
  pinMode(RECEIVE, INPUT);
  pinMode(TRANSMIT_LINE, OUTPUT);
  digitalWrite(TRANSMIT_LINE, 1);
  //Serial connection setup for computer console
  Serial.println("Board connected to pc");
  // Trigger interrupt on rising and falling edge (CHANGE)
  attachInterrupt(digitalPinToInterrupt(RECEIVE), CHANGE_ISR, CHANGE);

  // Timer Setup
  IntervalTimer = timerBegin(0, 80, true);  // Timer for monitoring system state
  timerAttachInterrupt(IntervalTimer, &onTimer, true);
  timerAlarmWrite(IntervalTimer, STATE_ALARM, true);
  timerAlarmEnable(IntervalTimer);
  
  ManchesterMsgTimer = timerBegin(1,80,true); // Timer for sending messages
  timerAttachInterrupt(ManchesterMsgTimer, &SendHalfBit, true);
  timerAlarmWrite(ManchesterMsgTimer, TX_ALARM, true);

  ReceiveTimer = timerBegin(2,80,true); // Timer for receiving messages
  timerStart(IntervalTimer);
  resetHeader();
}

void loop() {
  stateUpdate();
  if((SYS_STATE == COLLISION && getTXEnable())){
    setCurrentTransmit(0);
    timerAlarmDisable(ManchesterMsgTimer);
    setTXEnable(false);
    setTXWaiting(false);
    digitalWrite(TRANSMIT_LED, 0);
    digitalWrite(TRANSMIT_LINE, 1);
    int randomDelay = (random(0,200)*5);
    while(SYS_STATE == COLLISION){
      stateUpdate();
    }
    timeOfCollision = millis();
    setTXCollisionDelay(randomDelay);
    Serial.println(randomDelay);
    CollisionWaiting = true;

  } else if((CollisionWaiting)&&(millis()-timeOfCollision == getTXCollisionDelay())){
    CollisionWaiting = false;
    setTXCollisionDelay(0);
    setTXEnable(true);
    timerAlarmEnable(ManchesterMsgTimer);
    timeOfCollision = 0;
    Serial.println("retransmit");

  }else if(!getTXEnable()){
    timerAlarmDisable(ManchesterMsgTimer);
    digitalWrite(TRANSMIT_LED, 0);
    digitalWrite(TRANSMIT_LINE, 1);
    setCurrentTransmit(0);

  } else if(getTXWaiting()) {
    if(txCheck(ManchesterMsgTimer)){
      Serial.printf("Transmitting Message to: %s\n", parsedMessage[1]);
      Serial.printf("%s %s\n","Message being transmitted:",parsedMessage[2]);
    }
    setTXEnable(true);
    setTXWaiting(false);
  } else if(!getTXEnable()){
    timerAlarmDisable(ManchesterMsgTimer);
    digitalWrite(TRANSMIT_LED, 0);
    digitalWrite(TRANSMIT_LINE, 1);
    setCurrentTransmit(0);

  }
  if(Serial.available()){
    message = Serial.readStringUntil('\n');
    consoleParser(message);
    if(parsedMessage[0].compareTo("tx") == 0){
      Serial.printf("tx ready and will transmit on next idle\n");
      // Create header string
      std::string preamble = "01010101";
      std::string source = "00110100";
      std::string destination = std::bitset<8>(std::stoi(parsedMessage[1].c_str(),NULL,16)).to_string();
      std::string length = std::bitset<8>(strlen(parsedMessage[2].c_str())).to_string();
      std::string crcFlag = "00000001";
      std::string msgWithHeader = preamble+source+destination+length+crcFlag;
      setBin(msgWithHeader);
      std::string txCRC = std::bitset<8>(computeCRC8((uint8_t *)parsedMessage[2].c_str(), parsedMessage[2].length())).to_string();
      // Send message to binbuffer
      printf("HERE\n");
      auto start = high_resolution_clock::now();
      char cstr_msg[1500];
      strcpy(cstr_msg, parsedMessage[2].c_str());
      printf("HERE\n");
      encrypt_message(cstr_msg,strlen(cstr_msg));
      printf("HERE\n");
      printf("CSTR_MSG: %s\n", cstr_msg);
      stringToBuffer(cstr_msg);
      //stringToBuffer(parsedMessage[2].c_str());
      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<microseconds>(stop - start);
      Serial.printf("TIME ELAPSED (for send): %d", duration);
      setBin(txCRC);
      setCurrentTransmit(0);
      //enables ready
      setTXEnable(true);
      setTXWaiting(true);
    } else if(parsedMessage[0].compareTo("stop") == 0){
      timerAlarmDisable(ManchesterMsgTimer);
      digitalWrite(TRANSMIT_LED, 0);
      setTXEnable(false);
      Serial.printf("tx stopped\n");
    } else if(parsedMessage[0].compareTo("") == 0){
      // This is where the user will decrypt the message
      auto start = high_resolution_clock::now();
      printMsg();
      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<microseconds>(stop - start);
      Serial.printf("TIME ELAPSED (for receive): %d", duration);
      clearMsg();
    } else {
      Serial.printf("Not a recognised command!\n");
    }
  } else {
    message = "";
  }
}
